# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=pipewire
pkgver=0.3.34
pkgrel=2
pkgdesc="Multimedia processing graphs"
url="https://pipewire.org/"
arch="all !s390x !mips64" # unit tests fail on big-endian
license="LGPL-2.1-or-later"
# The launch script /usr/libexec/pipewire-launcher uses an argument that is unsupported by Busybox's pkill
# So we use pkill provided by procps instead
depends="procps"
makedepends="
	alsa-lib-dev
	avahi-dev
	bash
	bluez-dev
	dbus-dev
	doxygen
	eudev-dev
	fdk-aac-dev
	ffmpeg-dev
	glib-dev
	graphviz
	gst-plugins-base-dev
	gstreamer-dev
	jack-dev
	libfreeaptx-dev
	libldac-dev
	libusb-dev
	libx11-dev
	meson
	ncurses-dev
	pulseaudio-dev
	sbc-dev
	sdl2-dev
	vulkan-loader-dev
	xmltoman
	"
subpackages="
	$pkgname-dev
	$pkgname-doc
	$pkgname-alsa
	$pkgname-pulse
	$pkgname-jack
	$pkgname-media-session:media_session
	gst-plugin-pipewire:gst_plugin
	$pkgname-zeroconf
	$pkgname-spa-bluez
	$pkgname-spa-ffmpeg
	$pkgname-spa-vulkan
	$pkgname-tools
	$pkgname-spa-tools:spa_tools
	$pkgname-lang
	"
install="$pkgname.post-upgrade"
source="https://gitlab.freedesktop.org/PipeWire/pipewire/-/archive/$pkgver/pipewire-$pkgver.tar.gz
	$pkgname-alsa-Free-global-state.patch::https://github.com/PipeWire/pipewire/commit/0a5a4c046d10c0a53c7d2120a523cc28663ad73b.patch
	0001-network-audio-delay-fix.patch
	pipewire.desktop
	pipewire-launcher.sh
	"

case "$CARCH" in
	# Limited by webrtc-audio-processing-dev
	x86 | x86_64 | aarch64)
		makedepends="$makedepends webrtc-audio-processing-dev"
		subpackages="$subpackages $pkgname-echo-cancel:echo_cancel"
	;;
esac

build() {
	abuild-meson \
		-Ddocs=disabled \
		-Dman=enabled \
		-Dgstreamer=enabled \
		-Dexamples=enabled \
		-Dffmpeg=enabled \
		-Dsystemd=disabled \
		-Dvulkan=enabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	touch "$pkgdir"/usr/share/pipewire/media-session.d/with-alsa

	install -Dm644 "$srcdir"/pipewire.desktop -t "$pkgdir"/etc/xdg/autostart/
	install -Dm755 "$srcdir"/pipewire-launcher.sh "$pkgdir"/usr/libexec/pipewire-launcher
}

alsa() {
	pkgdesc="ALSA support for pipewire"
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/alsa-lib
	amove usr/share/alsa/alsa.conf.d
	amove usr/share/pipewire/media-session.d/with-alsa

	cd "$subpkgdir"

	mkdir -p etc/alsa
	cp -r usr/share/alsa/alsa.conf.d etc/alsa/
}

pulse() {
	pkgdesc="Pulseaudio support for pipewire"
	depends="$pkgname-media-session"
	provides="pulseaudio=$pkgver-r$pkgrel pulseaudio-bluez=$pkgver-r$pkgrel pulseaudio-alsa=$pkgver-r$pkgrel"
	provider_priority=1

	amove usr/bin/pipewire-pulse
	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-protocol-pulse.so
	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-pulse-tunnel.so
	amove usr/share/pipewire/media-session.d/with-pulseaudio
	amove usr/share/pipewire/pipewire-pulse.conf

}

jack() {
	pkgdesc="JACK support for pipewire"
	depends=""

	amove usr/lib/pipewire-*/jack
	amove usr/bin/pw-jack
	amove usr/lib/spa-*/jack/libspa-jack.so
	amove usr/share/pipewire/media-session.d/with-jack
	amove usr/share/pipewire/jack.conf
}

media_session() {
	pkgdesc="$pkgdesc - Session manager"
	depends=""

	amove usr/bin/pipewire-media-session
	amove usr/share/pipewire/media-session.d
}

gst_plugin() {
	pkgdesc="Multimedia graph framework - PipeWire plugin"
	depends="$pkgname-media-session gst-plugins-base"

	amove usr/lib/gstreamer-1.0
}

echo_cancel() {
	pkgdesc="WebRTC-based echo canceller module for PipeWire"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-echo-cancel.so
}

zeroconf() {
	pkgdesc="$pkgdesc - Zeroconf support"
	depends=""
	provides="pulseaudio-zeroconf=$pkgver-r$pkgrel"
	provider_priority=1

	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-zeroconf-discover.so
}

bluez() {
	pkgdesc="PipeWire BlueZ5 SPA plugin (Bluetooth)"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/spa-*/bluez5
}

ffmpeg() {
	pkgdesc="PipeWire FFmpeg SPA plugin"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/spa-*/ffmpeg
}

vulkan() {
	pkgdesc="PipeWire Vulkan SPA plugin"
	depends=""

	amove usr/lib/spa-*/vulkan
}

tools() {
	pkgdesc="PipeWire tools"
	depends="$pkgname=$pkgver-r$pkgrel"
	replaces="$pkgname"  # for backward compatibility

	amove usr/bin/pw-*
}

spa_tools() {
	pkgdesc="PipeWire SPA tools"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/bin/spa-*
}

sha512sums="
cbf8eb410d2cd1923e67ac9ee9eca0e74567bb7e8ea407a536e64b3ec27b5bbb1e7fdd74a5a7cbffbac3f9996d8a08f8c723401fb44cb2920f044a3a2f3a6c18  pipewire-0.3.34.tar.gz
7f28925e8f7be646b67f823b76d68930351ad8c12f651f9cd9a2c701784305984ab43e2073ad53e52bc3145c4712e2654ea40f84a103ef2affffd3af35f1a26b  pipewire-alsa-Free-global-state.patch
96ebbcee762b2f5674723ebd574b70a6aca42511ea620be981066f40fe6afd79801f8f68dab6fc65d9d4582aec268d78028b3f67833c529a8655aebc7093f563  0001-network-audio-delay-fix.patch
d5d8bc64e42715aa94296e3e26e740142bff7f638c7eb4fecc0301e46d55636d889bdc0c0399c1eb523271b20f7c48cc03f6ce3c072e0e8576c821ed1ea0e3dd  pipewire.desktop
51a1733571a09f73f30a4e98424dfdf96b2f73036843738f9579bb3fff08019d9136ee0e434124e46b6e1066a7f7aa1cd9580aedd7ec17573031be435ae1201b  pipewire-launcher.sh
"
