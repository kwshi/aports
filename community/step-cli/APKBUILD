# Contributor: Adrian L Lange <alpine@p3lim.net>
# Contributor: Charles Wimmer <charles@wimmer.net>
# Contributor: Dermot Bradley <dermot_bradley@yahoo.com>
# Maintainer: Dermot Bradley <dermot_bradley@yahoo.com>
pkgname=step-cli
pkgver=0.17.1
pkgrel=0
pkgdesc="Zero trust swiss army knife that integrates with step-ca for automated certificate management"
url="https://github.com/smallstep/cli"
arch="all"
license="Apache-2.0"
makedepends="
	bash
	go
	go-bindata
	"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/smallstep/cli/archive/v$pkgver.tar.gz
	"
builddir="$srcdir/cli-$pkgver"

build() {
	make build
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm644 autocomplete/bash_autocomplete \
		"$pkgdir"/usr/share/bash-completion/completions/step

	install -Dm644 autocomplete/zsh_autocomplete \
		"$pkgdir"/usr/share/zsh/site-functions/_step
}

sha512sums="
23022b2f7367b73ac4feda5d1796c8aa0d3e89d0f562386a3f22165cec24ea5d37c400d4c77cb07e2f84196dac55d63e03b494cee554b424afc848fa1735ec7c  step-cli-0.17.1.tar.gz
"
